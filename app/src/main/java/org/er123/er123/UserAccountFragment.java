package org.er123.er123;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.amazonaws.mobile.config.AWSConfiguration;
import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool;

public class UserAccountFragment extends Fragment {

    Switch switchEditDetails;
    TextView txtEditFName, txtEditLName, txtEditAddress1, txtEditAddress2, txtEditCity, txtEditPostcode;
    TextView txtEditMedication, txtEditAllergies, txtEditMedicalInfo, txtEditGP;
    EditText txtEditDOB;
    AlertDialog dialogFName, dialogLName, dialogAddress1, dialogAddress2, dialogCity, dialogPostcode;
    AlertDialog dialogMedication, dialogAllergies, dialogMedicalInfo, dialogGP;
    EditText editFName, editLName, editAddress1, editAddress2, editCity, editPostcode;
    EditText editMedication, editAllergies, editMedicalInfo, editGP;
    Button submitDetails, backButton;

    AWSAppSyncClient mAWSAppSyncClient;


    Spinner spinner;
    String gender[] = {" ", "Male", "Female", "Other"};
    ArrayAdapter<String> arrayAdapter;

    public UserAccountFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.user_account_fragment, container, false);

        mAWSAppSyncClient = AWSAppSyncClient.builder().context(getContext()).awsConfiguration(new AWSConfiguration(getContext())).build();

        submitDetails = rootView.findViewById(R.id.updateAccount);
        backButton = rootView.findViewById(R.id.backAccount);

        switchEditDetails = (Switch)rootView.findViewById(R.id.switchEdit);
        txtEditFName = (TextView)rootView.findViewById(R.id.editFName);
        txtEditLName = (TextView)rootView.findViewById(R.id.editLName);
        txtEditAddress1 = (TextView)rootView.findViewById(R.id.editAddressLine1);
        txtEditAddress2 = (TextView)rootView.findViewById(R.id.editAddressLine2);
        txtEditCity = (TextView)rootView.findViewById(R.id.editCity);
        txtEditPostcode = (TextView)rootView.findViewById(R.id.editPostCode);
        txtEditMedication = rootView.findViewById(R.id.editMedications);
        txtEditAllergies = rootView.findViewById(R.id.editAllergies);
        txtEditMedicalInfo = rootView.findViewById(R.id.editExistingMedical);
        txtEditDOB = rootView.findViewById(R.id.etSelectDate);
        txtEditDOB.setText("21/05/1983");
        txtEditGP = rootView.findViewById(R.id.editRegisteredGP);
        txtEditGP.setText("Dr GP");

        dialogFName = new AlertDialog.Builder(this.getActivity()).create();
        dialogLName = new AlertDialog.Builder(this.getActivity()).create();
        dialogAddress1 = new AlertDialog.Builder(this.getActivity()).create();
        dialogAddress2 = new AlertDialog.Builder(this.getActivity()).create();
        dialogCity = new AlertDialog.Builder(this.getActivity()).create();
        dialogPostcode = new AlertDialog.Builder(this.getActivity()).create();
        dialogMedication = new AlertDialog.Builder(this.getActivity()).create();
        dialogAllergies = new AlertDialog.Builder(this.getActivity()).create();
        dialogMedicalInfo = new AlertDialog.Builder(this.getActivity()).create();
        dialogGP = new AlertDialog.Builder(this.getActivity()).create();
        editFName = new EditText(this.getActivity());
        editLName = new EditText(this.getActivity());
        editAddress1 = new EditText(this.getActivity());
        editAddress2 = new EditText(this.getActivity());
        editCity = new EditText(this.getActivity());
        editPostcode = new EditText(this.getActivity());
        editMedication = new EditText(this.getActivity());
        editAllergies = new EditText(this.getActivity());
        editMedicalInfo = new EditText(this.getActivity());
        editGP = new EditText(this.getActivity());

        txtEditFName.setText("Bruce");
        dialogFName.setTitle("First Name");
        dialogFName.setView(editFName);

        txtEditLName.setText("Jones");
        dialogLName.setTitle("Last Name");
        dialogLName.setView(editLName);

        txtEditAddress1.setText("64 Zoo Lane");
        dialogAddress1.setTitle("Address Line 1");
        dialogAddress1.setView(editAddress1);

        txtEditAddress2.setText("Charles Street");
        dialogAddress2.setTitle("Address Line 2");
        dialogAddress2.setView(editAddress2);

        txtEditCity.setText("Manchester");
        dialogCity.setTitle("City");
        dialogCity.setView(editCity);

        txtEditPostcode.setText("ML3 5JR");
        dialogPostcode.setTitle("Postcode");
        dialogPostcode.setView(editPostcode);

        txtEditMedicalInfo.setText("Medical Info");
        dialogMedicalInfo.setTitle("Medical Information");
        dialogMedicalInfo.setView(editMedicalInfo);

        txtEditMedication.setText("Medication");
        dialogMedication.setTitle("Medication");
        dialogMedication.setView(editMedication);

        txtEditAllergies.setText("Pills");
        dialogAllergies.setTitle("Allergies");
        dialogAllergies.setView(editAllergies);

        dialogGP.setTitle("Register GP");
        dialogGP.setView(editGP);

        switchEditDetails.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (switchEditDetails.isChecked())
                {
                    txtEditFName.setEnabled(true);
                    txtEditLName.setEnabled(true);
                    txtEditAddress1.setEnabled(true);
                    txtEditAddress2.setEnabled(true);
                    txtEditCity.setEnabled(true);
                    txtEditPostcode.setEnabled(true);
                    txtEditAllergies.setEnabled(true);
                    txtEditMedicalInfo.setEnabled(true);
                    txtEditMedication.setEnabled(true);
                    txtEditGP.setEnabled(true);


                    dialogFName.setButton(DialogInterface.BUTTON_POSITIVE, "SAVE DETAILS", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Displays the edited details
                            txtEditFName.setText(editFName.getText());
                        }
                    });

                    dialogLName.setButton(DialogInterface.BUTTON_POSITIVE, "SAVE DETAILS", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Displays the edited details
                            txtEditLName.setText(editLName.getText());
                        }
                    });

                    dialogAddress1.setButton(DialogInterface.BUTTON_POSITIVE, "SAVE DETAILS", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Displays the edited details
                            txtEditAddress1.setText(editAddress1.getText());
                        }
                    });

                    dialogAddress2.setButton(DialogInterface.BUTTON_POSITIVE, "SAVE DETAILS", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Displays the edited details
                            txtEditAddress2.setText(editAddress2.getText());
                        }
                    });

                    dialogCity.setButton(DialogInterface.BUTTON_POSITIVE, "SAVE DETAILS", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Displays the edited details
                            txtEditCity.setText(editCity.getText());
                        }
                    });

                    dialogPostcode.setButton(DialogInterface.BUTTON_POSITIVE, "SAVE DETAILS", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Displays the edited details
                            txtEditPostcode.setText(editPostcode.getText());
                        }
                    });

                    dialogMedication.setButton(DialogInterface.BUTTON_POSITIVE, "SAVE DETAILS", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Displays the edited details
                            txtEditMedication.setText(editMedication.getText());
                        }
                    });

                    dialogMedicalInfo.setButton(DialogInterface.BUTTON_POSITIVE, "SAVE DETAILS", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Displays the edited details
                            txtEditMedicalInfo.setText(editMedicalInfo.getText());
                        }
                    });

                    dialogAllergies.setButton(DialogInterface.BUTTON_POSITIVE, "SAVE DETAILS", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Displays the edited details
                            txtEditAllergies.setText(editAllergies.getText());
                        }
                    });

                    dialogGP.setButton(DialogInterface.BUTTON_POSITIVE, "SAVE DETAILS", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Displays the edited details
                            txtEditGP.setText(editGP.getText());
                        }
                    });



                    txtEditFName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            editFName.setText(txtEditFName.getText());
                            dialogFName.show();
                        }
                    });

                    txtEditLName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            editLName.setText(txtEditLName.getText());
                            dialogLName.show();
                        }
                    });

                    txtEditAddress1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            editAddress1.setText(txtEditAddress1.getText());
                            dialogAddress1.show();
                        }
                    });

                    txtEditAddress2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            editAddress2.setText(txtEditAddress2.getText());
                            dialogAddress2.show();
                        }
                    });

                    txtEditCity.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            editCity.setText(txtEditCity.getText());
                            dialogCity.show();
                        }
                    });

                    txtEditPostcode.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            editPostcode.setText(txtEditPostcode.getText());
                            dialogPostcode.show();
                        }
                    });

                    txtEditMedication.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            editMedication.setText(txtEditMedication.getText());
                            dialogMedication.show();
                        }
                    });

                    txtEditMedicalInfo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            editMedicalInfo.setText(txtEditMedicalInfo.getText());
                            dialogMedicalInfo.show();
                        }
                    });

                    txtEditAllergies.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            editAllergies.setText(txtEditAllergies.getText());
                            dialogAllergies.show();
                        }
                    });

                    txtEditGP.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            editGP.setText(txtEditGP.getText());
                            dialogGP.show();
                        }
                    });


                }
                else
                {
                    txtEditFName.setEnabled(false);
                    txtEditLName.setEnabled(false);
                    txtEditAddress1.setEnabled(false);
                    txtEditAddress2.setEnabled(false);
                    txtEditCity.setEnabled(false);
                    txtEditPostcode.setEnabled(false);
                    txtEditAllergies.setEnabled(false);
                    txtEditMedicalInfo.setEnabled(false);
                    txtEditMedication.setEnabled(false);
                    txtEditGP.setEnabled(false);
                }


            }
        });


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = getActivity()
                        .getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, new MapFragment());
                fragmentTransaction.commit();
            }
        });

        submitDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String editedFName = txtEditFName.getText().toString();
                String editedLName = txtEditLName.getText().toString();
                String editedMedicalInfo = txtEditMedicalInfo.getText().toString();
                String editedMedication = txtEditMedication.getText().toString();
                String editedAllergies = txtEditAllergies.getText().toString();
                String editedDOB = txtEditDOB.getText().toString();
                String editedGender = spinner.getSelectedItem().toString();

                Bundle bundle = new Bundle();
                bundle.putString("Fname", editedFName);
                bundle.putString("Lname", editedLName);
                bundle.putString("MedicalInfo", editedMedicalInfo);
                bundle.putString("Medication", editedMedication);
                bundle.putString("Allergies", editedAllergies);
                bundle.putString("DOB", editedDOB);
                bundle.putString("Gender", editedGender);

                FragmentTransaction fragmentTransaction = getActivity()
                        .getSupportFragmentManager().beginTransaction();

                MedicalFragment medicalFragment = new MedicalFragment();
                medicalFragment.setArguments(bundle);

                fragmentTransaction.replace(R.id.fragment_container, medicalFragment);
                fragmentTransaction.commit();
            }
        });



        spinner = (Spinner)rootView.findViewById(R.id.editGender);
        arrayAdapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_list_item_1, gender);
        spinner.setAdapter(arrayAdapter);

        return rootView;

    }



}
