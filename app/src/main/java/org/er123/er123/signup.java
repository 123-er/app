package org.er123.er123;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.google.android.material.textfield.TextInputEditText;

import java.util.regex.Pattern;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.results.SignUpResult;
import com.amazonaws.mobile.client.results.UserCodeDeliveryDetails;

import java.util.HashMap;
import java.util.Map;

public class signup extends AppCompatActivity {

    private static final Pattern PHONENUMBER_PATTERN =
            Pattern.compile("^[+]44[0-9]{10}$"); //starts with +44



    private TextInputEditText textInputFName, textInputLName, textPhoneNumber, textEmail, textPassword, textConfirmPassword;
    private Button signUp;

    Spinner spinner;
    String gender[] = {"", "Male", "Female", "Other"};
    ArrayAdapter<String> arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);

        textInputFName = (TextInputEditText) findViewById(R.id.inputFName);
        textInputLName = (TextInputEditText) findViewById(R.id.inputLName);
        textPhoneNumber = (TextInputEditText) findViewById(R.id.inputPhoneNum);
        textEmail = (TextInputEditText) findViewById(R.id.inputEmail);
        textPassword = (TextInputEditText) findViewById(R.id.inputPassword);
        textConfirmPassword = (TextInputEditText) findViewById(R.id.inputConfirmPassword);

        signUp = (Button) findViewById(R.id.btnSignUp);

        signUp.setOnClickListener(this::submitDetails);

        spinner = (Spinner) findViewById(R.id.inputGender);

        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, gender);
        spinner.setAdapter(arrayAdapter);
    }

    private boolean presenceCheck() {

        String fNameInput = textInputFName.getText().toString().trim();
        String lNameInput = textInputLName.getText().toString().trim();



        if (fNameInput.isEmpty()) {
            textInputFName.setError("Field can't be empty");
            return false;
        }
        else if (lNameInput.isEmpty()){
            textInputLName.setError("Field can't be empty");
            return false;
        }
        else{
            textInputFName.setError(null);
            return true;
        }

    }

    private boolean validateEmail(){
        String emailInput = textEmail.getText().toString().trim();

        if (emailInput.isEmpty()){
            textEmail.setError("Please enter your email");
            return false;
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()){
            textEmail.setError("Email is not in the correct format");
            return false;
        }
        else {
            textEmail.setError(null);
            return true;
        }
    }

    private boolean validatePhoneNumber(){
        String phoneNumberInput = textPhoneNumber.getText().toString().trim();

        if (phoneNumberInput.isEmpty()){
            textPhoneNumber.setError("Please enter a phone number");
            return false;
        }
        else if (!PHONENUMBER_PATTERN.matcher(phoneNumberInput).matches()){
            textPhoneNumber.setError("Ensure that the phone number is in the correct format, starting with +44");
            return false;
        }
        else {
            textPhoneNumber.setError(null);
            return true;
        }
    }




    public void submitDetails(View view){

        if ((!presenceCheck()) || (!validatePhoneNumber()) || (!validateEmail()) || (!global.validatePassword(textConfirmPassword, textPassword))){
            return;
        } else {
            signup(view);
        }

    }

    private void makeToast(String string) {
        Toast.makeText(getApplicationContext(), string, Toast.LENGTH_SHORT).show();
    }

    public void signup(View v) {
        EditText email = findViewById(R.id.inputEmail);
        EditText password = findViewById(R.id.inputPassword);
        EditText given_name = findViewById(R.id.inputFName);
        EditText family_name = findViewById(R.id.inputLName);
        Spinner gender = findViewById(R.id.inputGender);
        EditText phone_number = findViewById(R.id.inputPhoneNum);

        final Map<String, String> attributes = new HashMap<>();

        attributes.put("given_name", given_name.getText().toString());
        attributes.put("family_name", family_name.getText().toString());
        attributes.put("gender", gender.getSelectedItem().toString());
        attributes.put("phone_number", phone_number.getText().toString());



        AWSMobileClient.getInstance().signUp(email.getText().toString(), password.getText().toString(), attributes, null, new Callback<SignUpResult>() {
            @Override
            public void onResult(SignUpResult result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(this.getClass().getSimpleName(), "Sign-up callback state: " + result.getConfirmationState());
                        if (!result.getConfirmationState()) {
                            final UserCodeDeliveryDetails details = result.getUserCodeDeliveryDetails();
                            makeToast("Confirm sign-up with: " + details);

                            Intent intent = new Intent(signup.this, login.class);
                            intent.putExtra("email", email.getText().toString());
                            startActivity(intent);
                        } else {
                            makeToast("Sign-up done");
                            setResult(RESULT_OK);
                            finish();
                        }
                    }
                });
            }

            @Override
            public void onError(Exception e) {
                Log.e(this.getClass().getSimpleName(), "Sign-up error", e);
            }
        });
    }

    public void resend(View view) {
        EditText email = findViewById(R.id.inputEmail);

        AWSMobileClient.getInstance().resendSignUp(email.getText().toString(), new Callback<SignUpResult>() {
            @Override
            public void onResult(SignUpResult signUpResult) {
                Log.i(this.getClass().getSimpleName(), "A verification code has been sent via" +
                        signUpResult.getUserCodeDeliveryDetails().getDeliveryMedium()
                        + " at " +
                        signUpResult.getUserCodeDeliveryDetails().getDestination());
            }

            @Override
            public void onError(Exception e) {
                Log.e(this.getClass().getSimpleName(), e.toString());
            }
        });

    }

    public void back(View view) {
        finish();
    }
}

