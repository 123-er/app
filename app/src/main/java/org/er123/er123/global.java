package org.er123.er123;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.google.android.material.textfield.TextInputEditText;

import java.util.regex.Pattern;

public class global extends AppCompatActivity {

    public static void makeToast(String string, Context context) {
        Toast.makeText(context, string, Toast.LENGTH_SHORT).show();
    }

    public static String getInput(EditText element) {
        return element.getText().toString();
    }



    private static final Pattern PASSWORD_PATTERN =
            Pattern.compile("^" +
                    "(?=.*[0-9])" +  //at least 1 digit
                    "(?=.*[a-z])" + //at least 1 lower case letter
                    "(?=.*[A-Z])" + //at least 1 upper case letter
                    "(?=.*[@#$%^&+=!])" + //at least 1 special character
                    ".{6,}" + //at least 6 characters
                    "$");

    public static boolean validatePassword(EditText textConfirmPassword, EditText textPassword){
        String passwordInput = textPassword.getText().toString().trim();
        String confirmPasswordInput = textConfirmPassword.getText().toString().trim();

        if (passwordInput.isEmpty()) {
            textPassword.setError("Please enter a password");
            return false;
        }
        else if (!PASSWORD_PATTERN.matcher(passwordInput).matches()) {
            textPassword.setError("Password is too weak");
            return false;
        }
        else if (!passwordInput.equals(confirmPasswordInput)){
            textConfirmPassword.setError("Password does not match");
            return false;
        }
        else {
            textPassword.setError(null);
            return true;
        }
    }
}
