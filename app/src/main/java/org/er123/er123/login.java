package org.er123.er123;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.results.ForgotPasswordResult;
import com.amazonaws.mobile.client.results.SignInResult;

public class login extends AppCompatActivity {

    Button signUpButton;
    Intent thisIntent = getIntent();

    private EditText username;
    private EditText password;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        username = findViewById(R.id.entryUsername);
        password = findViewById(R.id.entryPassword);
        textView = findViewById(R.id.textError);

        signUpButton = findViewById(R.id.signup);
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(login.this, signup.class);
                startActivityForResult(intent, 1);

                try {
                    username.setText(thisIntent.getStringExtra("email"));
                } catch (NullPointerException e) {
                    Log.e("NotFromSignup: ", e.toString());
                }
            }
        });
    }

    public void loginButton(View view) {

        String errorMessageUserName = "Error: Username is empty";
        String errorMessagePassword = "Error: Password is empty";
        String errorMessageUserPassword = "Error: Username and Password is empty";

        String usernameText = username.getText().toString();
        String passwordText = password.getText().toString();

        ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setMessage("Please Wait Whilst We Verify ...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

        if (usernameText.equals("") && passwordText.equals("")){
            Toast.makeText(getApplicationContext(),errorMessageUserPassword,Toast.LENGTH_LONG).show();
        }
        else if (passwordText.equals("")){
            Toast.makeText(getApplicationContext(),errorMessagePassword,Toast.LENGTH_LONG).show();
        }
        else if (usernameText.equals("")){
            Toast.makeText(getApplicationContext(),errorMessageUserName,Toast.LENGTH_LONG).show();
        }
        else if (usernameText.equals("123erdevelop@gmail.com") && passwordText.equals("Password1!")) {
            startActivity(new Intent(login.this, homeScreen.class));
        }


        AWSMobileClient.getInstance().signIn(usernameText, passwordText, null, new Callback<SignInResult>() {
            @Override
            public void onResult(SignInResult result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(this.getClass().getSimpleName(), "Sign-in callback state" + result.getSignInState());
                        switch (result.getSignInState()) {
                            case DONE:
                                global.makeToast("Signed in", getApplicationContext());
                                progress.dismiss();
                                break;
                            case SMS_MFA:
                                global.makeToast("Please confirm with SMS Code", getApplicationContext());
                                progress.dismiss();
                                break;
                            case NEW_PASSWORD_REQUIRED:
                                global.makeToast("Please confirm sign-in with new password", getApplicationContext());
                                progress.dismiss();
                                break;
                            default:
                                global.makeToast("Unsupported sign-in confirmation: " + result.getSignInState(), getApplicationContext());
                                progress.dismiss();
                        }
                    }
                });
            }

            @Override
            public void onError(Exception e) {
                Log.e(this.getClass().getSimpleName(), "sign in error", e);
                progress.dismiss();
            }
        });
    }

    public void forgotPassword(View view) {
        EditText email = findViewById(R.id.inputEmail);
        Intent intent = new Intent(login.this, changePassword.class);
        intent.putExtra("email", email.getText().toString());
        startActivity(intent);
    }

}
