package org.er123.er123;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.amplify.generated.graphql.CreateUserDataMutation;
import com.amazonaws.auth.AWSCognitoIdentityProvider;
import com.amazonaws.auth.AnonymousAWSCredentials;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobile.auth.core.IdentityManager;
import com.amazonaws.mobile.config.AWSConfiguration;
import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserAttributes;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GetDetailsHandler;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cognitoidentity.AmazonCognitoIdentityClient;
import com.amazonaws.services.cognitoidentityprovider.AmazonCognitoIdentityProvider;
import com.amazonaws.services.cognitoidentityprovider.AmazonCognitoIdentityProviderClient;
import com.amazonaws.services.cognitoidentityprovider.model.AdminGetUserRequest;
import com.amazonaws.services.cognitoidentityprovider.model.AdminGetUserResult;
import com.amazonaws.services.cognitoidentityprovider.model.GetUserRequest;
import com.amazonaws.services.cognitoidentityprovider.model.GetUserResult;
import com.apollographql.apollo.GraphQLCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.results.SignUpResult;
import com.amazonaws.mobile.client.results.UserCodeDeliveryDetails;
import com.google.android.material.tabs.TabLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Nonnull;

import type.CreateUserDataInput;

public class InitialUserDetails extends AppCompatActivity {

    private Button backUserDetails;
    private Button confirmUserDetails;
    private EditText address, DOB, postcode, city, county , country, existingConditions, gp, allergy, medication;
    private TextView displayName;

    Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date;


    private AWSAppSyncClient mAWSAppSyncClient;
    private AWSConfiguration AWSConfiguration;
    private AmazonCognitoIdentityClient amazonCognitoIdentityClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.initial_user_details);

        mAWSAppSyncClient = AWSAppSyncClient.builder().context(getApplicationContext()).awsConfiguration(new AWSConfiguration(getApplicationContext())).build();

        displayName = findViewById(R.id.displayUser);
        backUserDetails = findViewById(R.id.cancelUserDetails);
        confirmUserDetails = findViewById(R.id.confirmUserDetails);
        address = findViewById(R.id.inputAddress);
        DOB = findViewById(R.id.userSelectDOB);
        postcode = findViewById(R.id.inputPostcode);
        city = findViewById(R.id.inputCity);
        county = findViewById(R.id.inputCounty);
        country = findViewById(R.id.inputCountry);
        gp = findViewById(R.id.inputGP);
        existingConditions = findViewById(R.id.inputExistingMedical);
        allergy = findViewById(R.id.inputAllergies);
        medication = findViewById(R.id.inputMedication);



        CognitoUserPool userPool = new CognitoUserPool(getApplicationContext(), "eu-west-2_zvrwddLE8", "2rflh38462qvjeqn52oj2nl6kk", "17129tt77dge3i63uknq48ftq8mtn65d6drpoq2ibdsmsfe3koue");
        CognitoUser cognitoUser = userPool.getCurrentUser();
        System.out.println(cognitoUser.getUserId());
        displayName.setText(cognitoUser.getUserId());

        backUserDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InitialUserDetails.this, homeScreen.class);
                startActivityForResult(intent, 1);
            }
        });

        confirmUserDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runMutation();
                Intent intent = new Intent(InitialUserDetails.this, homeScreen.class);
                startActivityForResult(intent, 1);
            }
        });




    }



    public void runMutation(){
        CreateUserDataInput createUserDataInput = CreateUserDataInput.builder()
                .userID("Test4")
                .dob("1973-03-21")
                .gender("test")
                .address1(address.getText().toString())
                .city(city.getText().toString())
                .postcode(postcode.getText().toString())
                .county(county.getText().toString())
                .country(country.getText().toString())
                .gp(gp.getText().toString())
                .medi_info(existingConditions.getText().toString())
                .medi_allergy(allergy.getText().toString())
                .medi_taken(medication.getText().toString())
                .build();
        mAWSAppSyncClient.mutate(CreateUserDataMutation.builder().input(createUserDataInput).build()).enqueue(mutationCallback);
    }

    private GraphQLCall.Callback<CreateUserDataMutation.Data> mutationCallback = new GraphQLCall.Callback<CreateUserDataMutation.Data>() {
        @Override
        public void onResponse(@Nonnull Response<CreateUserDataMutation.Data> response) {
            Log.i("Results","Added UserData");
        }

        @Override
        public void onFailure(@Nonnull ApolloException e) {
            Log.e("Error", e.toString());
        }
    };

}


