package org.er123.er123;

public class mockAPI {
    private static String[] usernames = {
            "josh",
            "elliot",
            "bruce"
    };

    private static String[] passwords = {
            "pass",
            "pass",
            "pass"
    };

    public static boolean authenticate(String username, String password) {
        for (int i = 0; i < usernames.length; i++) if (usernames[i].equals(username) && passwords[i].equals(password)) return true;
        return false;
    }
}
