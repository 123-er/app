package org.er123.er123;

import android.app.DatePickerDialog;
import android.util.Log;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputEditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class homeScreen extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;
    Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date;

    Calendar initialCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener initialDate;

    private EditText dobSpinner;
    private EditText initialDOBSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        BottomNavigationView bottomNav = findViewById(R.id.bottom_nav);
        bottomNav.setOnNavigationItemSelectedListener(navListener);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.drawable.logo);

        drawer = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.hamburger_nav);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        date = (DatePicker view1, int year, int month, int dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, month);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        };

        initialDate = (DatePicker view1, int year, int month, int dayOfMonth) -> {
            initialCalendar.set(Calendar.YEAR, year);
            initialCalendar.set(Calendar.MONTH, month);
            initialCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateInitialLabel();
        };



    }

    private void updateLabel() {
        dobSpinner = findViewById(R.id.etSelectDate);
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.UK);

        Log.e("DATE: ", sdf.format(myCalendar.getTime()));
        dobSpinner.setText(sdf.format(myCalendar.getTime()));

    }

    private void updateInitialLabel() {
        initialDOBSpinner = findViewById(R.id.userSelectDOB);
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.UK);

        Log.e("DATE: ", sdf.format(initialCalendar.getTime()));
        initialDOBSpinner.setText(sdf.format(initialCalendar.getTime()));

    }

    public void startCal(View view) {
        new DatePickerDialog(homeScreen.this, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    public void startNewCal(View view) {
        new DatePickerDialog(homeScreen.this, initialDate, initialCalendar
                .get(Calendar.YEAR), initialCalendar.get(Calendar.MONTH),
                initialCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    //Switches fragments when an item on the hamburger navigation is clicked
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item){

       switch (item.getItemId()){
           case R.id.uppernav_map:
               getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                       new MapFragment()).commit();
               break;
           case R.id.uppernav_medicalID:
               getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                       new MedicalFragment()).commit();
           break;

           case R.id.uppernav_emergency:
               getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                       new EmergencyFragment()).commit();
           break;

           case R.id.uppernav_account:
               getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                       new UserAccountFragment()).commit();

       }

       drawer.closeDrawer(GravityCompat.START);

        return true;
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }
        else
        {
            super.onBackPressed();
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;

                    switch (item.getItemId()){
                        case R.id.nav_medical:
                            selectedFragment = new MedicalFragment();
                            break;
                        case R.id.nav_emergency:
                            selectedFragment = new EmergencyFragment();
                            break;
                        case R.id.nav_chat:
                            selectedFragment = new ChatFragment();
                            break;

                    }

                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();

                    return true;
                }
            };



    public void signout(View view) {
        AWSMobileClient.getInstance().signOut();
        finish();
    }


}

