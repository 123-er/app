package org.er123.er123;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amazonaws.amplify.generated.graphql.GetUserDataQuery;
import com.amazonaws.amplify.generated.graphql.ListUserDataQuery;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.config.AWSConfiguration;
import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient;
import com.amazonaws.mobileconnectors.appsync.fetcher.AppSyncResponseFetchers;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.apollographql.apollo.GraphQLCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

import static com.amazonaws.mobile.auth.core.internal.util.ThreadUtils.runOnUiThread;

public class MedicalFragment extends Fragment {


    TextView displayName, displayDOB, displayGender, displayMedicalInfo, displayMedication, displayAllergies ;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_medical, container, false);

        Bundle bundle = getArguments();
        String fname = bundle.getString("Fname");
        String lname = bundle.getString("Lname");
        String medicalInfo = bundle.getString("MedicalInfo");
        String medication = bundle.getString("Medication");
        String allergies = bundle.getString("Allergies");
        String DOB = bundle.getString("DOB");
        String gender = bundle.getString("Gender");

        displayName = rootview.findViewById(R.id.txtName);
        displayGender = rootview.findViewById(R.id.txtDisplayGender);
        displayMedicalInfo = rootview.findViewById(R.id.txtDisplayMedicalInfo);
        displayMedication = rootview.findViewById(R.id.txtDisplayMedication);
        displayAllergies = rootview.findViewById(R.id.txtDisplayAllergies);
        displayDOB = rootview.findViewById(R.id.txtDisplayDOB);

        displayName.setText(fname +" " + lname);
        displayMedicalInfo.setText(medicalInfo);
        displayMedication.setText(medication);
        displayAllergies.setText(allergies);
        displayDOB.setText(DOB);
        displayGender.setText(gender);

        return rootview;
    }





}
