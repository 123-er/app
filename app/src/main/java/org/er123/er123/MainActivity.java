package org.er123.er123;



import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;


//amplify
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.UserStateDetails;
import com.amazonaws.mobile.client.UserStateListener;
import com.amazonaws.mobile.config.AWSConfiguration;
import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AWSMobileClient.getInstance().initialize(getApplicationContext(), new Callback<UserStateDetails>() {
                    @Override
                    public void onResult(UserStateDetails userStateDetails) {
                        Log.i("AmplifyInit", "onResult: " + userStateDetails.getUserState());
                        redirect(null);

                        runOnUiThread(() -> {
                            TextView loading = findViewById(R.id.loading);
                            loading.setVisibility(View.GONE);

                            TextView stuckText = findViewById(R.id.stuckText);
                            Button stuckButton = findViewById(R.id.stuckButton);
                            stuckText.setVisibility(View.VISIBLE);
                            stuckButton.setVisibility(View.VISIBLE);
                        });
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("AmplifyInit", "Initialization error.", e);
                        moveTaskToBack(true);
                        android.os.Process.killProcess(android.os.Process.myPid());
                        System.exit(1);
                    }
                }
        );
    }



    public void redirect(View view) {



        //check login state
        AWSMobileClient.getInstance().addUserStateListener(new UserStateListener() {
            @Override
            public void onUserStateChanged(UserStateDetails userStateDetails) {
                switch (userStateDetails.getUserState()){
                    case GUEST:
                        Log.i("userState", "user is in guest mode");
                        finishAffinity();
                        startActivity(new Intent(MainActivity.this, login.class));
                        finish();
                        break;
                    case SIGNED_OUT:
                        Log.i("userState", "user is signed out");
                        finishAffinity();
                        startActivity(new Intent(MainActivity.this, login.class));
                        finish();
                        break;
                    case SIGNED_IN:
                        Log.i("userState", "user is signed in");
                        startActivity(new Intent(MainActivity.this, InitialUserDetails.class));
                        finish();
                        break;
                    case SIGNED_OUT_USER_POOLS_TOKENS_INVALID:
                        Log.i("userState", "need to login again");
                        finishAffinity();
                        startActivity(new Intent(MainActivity.this, login.class));
                        finish();
                        break;
                    default:
                        Log.e("userState", "unsupported");
                }
            }
        });

    }


}
