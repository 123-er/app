package org.er123.er123;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.results.ForgotPasswordResult;

public class changePassword extends AppCompatActivity {

    String email;
    Intent thisIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        thisIntent = getIntent();
        email = thisIntent.getStringExtra("email");
    }

    public void back(View view) {
        finish();
    }

    public void changePassword(View view) {

        EditText password = findViewById(R.id.password);
        EditText passwordConfirm = findViewById(R.id.passwordConfirm);

        EditText confirmCode = findViewById(R.id.confirmCode);


        //check passwords
        if (global.validatePassword(password, passwordConfirm)) {
            AWSMobileClient.getInstance().confirmForgotPassword(password.getText().toString(), confirmCode.getText().toString(), new Callback<ForgotPasswordResult>() {
                @Override
                public void onResult(final ForgotPasswordResult result) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(this.getClass().getSimpleName(), "forgot password state: " + result.getState());
                            switch (result.getState()) {
                                case DONE:
                                    global.makeToast("Password changed successfully", getApplicationContext());
                                    break;
                                default:
                                    Log.e(this.getClass().getSimpleName(), "un-supported forgot password state");
                                    break;
                            }
                        }
                    });

                }

                @Override
                public void onError(Exception e) {
                    Log.e(this.getClass().getSimpleName(), "forgot password error", e);
                }
            });

        } else {
            global.makeToast("Error, passwords do not match.", getApplicationContext());
        }
    }
}


